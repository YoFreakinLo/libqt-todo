#include "todo.hpp"
#include <QMap>
#include <QTextStream>
#include <QStandardPaths>
#include <QFile>
#include <QDir>
#include <QRegularExpression>

static QTextStream qOut(stdout);
static QTextStream qIn(stdin);

todo::todo(const char *savePath, QObject *parent) :
    QObject(parent),
    quit(false)
{
	if(!savePath)
	{
		this->savePath = "./tasks/tasks";
	}
	else
	{
		this->savePath = savePath;
	}
	std::unique_ptr<QFile> saveFile = openSaveFile(QIODevice::ReadOnly);
	if(saveFile)
	{
		load(*saveFile);
	}
}

todo::todo(const QString &savePath, QObject *parent) :
    todo(savePath.toStdString().c_str(), parent)
{

}

todo::todo(const std::string &savePath, QObject *parent) :
    todo(savePath.c_str(), parent)
{

}

todo::~todo()
{
	auto saveFile = openSaveFile(QIODevice::WriteOnly);
	if(saveFile)
	{
		save(*saveFile);
	}
}

void todo::save(QFile &file) const
{
	QTextStream outputStream(&file);
	for(int i = 0; i < tasks.size(); i++)
	{
		tasks[i].save(outputStream);
	}
}

void todo::load(QFile &file)
{
	QTextStream inputStream(&file);
	while(!inputStream.atEnd())
	{
		Task t;
		t.load(inputStream);
		tasks.push_back(t);
	}
}

int todo::run()
{
	while(!quit)
	{
		handleCommand(readCommand());
	}

	return 0;
}

std::stringstream todo::getArguments() const
{
	return std::stringstream(qIn.readLine().toStdString());
}

QString todo::extractArgument(std::stringstream &args) const
{
	args.get();
	if(args.peek() != '"')
	{
		std::string string;
		args >> string;
		return QString(string.c_str());
	}
	args.get();
	std::stringstream ss;
	while(true)
	{
		char c = args.get();
		if(c == '"')
		{
			break;
		}
		ss << c;
	}
	args.get();
	return QString(ss.str().c_str());
}

QString todo::readCommand() const
{
	QString command;
	qIn >> command;
	return command;
}

void todo::handleCommand(const QString &command)
{
	if(command == "new")
	{
		std::stringstream args = getArguments();
		addTask(args);
	}
	else if(command == "progress")
	{
		std::stringstream args = getArguments();
		progressTask(args);
	}
	else if(command == "list")
	{
		list();
	}
	else if(command == "details")
	{
		std::stringstream args = getArguments();
		details(args);
	}
	else if(command == "help")
	{
		help();
	}
	else if(command == "quit")
	{
		quit = true;
	}
	else
	{
		qOut << "Command " << command << "not recognized" << endl;
	}
}

void todo::addTask(std::stringstream &args)
{
	QString title, desc, priority;
	title = extractArgument(args);
	desc = extractArgument(args);
	priority = extractArgument(args);
	Task::Priority p = stringToPriority(priority);

	tasks.push_back(Task(title, desc, p));
	qOut << "\nTask added succesfully\n" << endl;
}

void todo::progressTask(std::stringstream &args)
{
	qOut << endl;
	int index;
	args >> index;
	if(tasks[index].status() != Task::Status::FINISHED)
	{
		tasks[index].progress();
		qOut << "Task " << index << " made progress to " << statusToString(tasks[index].status()) << endl;
		return;
	}
	qOut << "Task is already finished" << endl;
}

void todo::list() const
{
	QMap<int, Task> statusOrder[static_cast<const int>(Task::Status::COUNT)];
	for(int i = 0; i < tasks.size(); i++)
	{
		statusOrder[static_cast<int>(tasks[i].status())].insert(i, tasks[i]);
	}

	qOut << "\n\tId\tTitle\tPrioirty" << endl;

	for(int i = 0; i < static_cast<int>(Task::Status::COUNT); i++)
	{
		qOut << statusToString(static_cast<Task::Status>(i)) << endl;
		foreach(int j, statusOrder[i].keys())
		{
			QString priority = priorityToString(statusOrder[i][j].priority());
			qOut << "\t" << j << "\t" << statusOrder[i][j].title().toStdString().c_str() << "\t" << priority << endl;
		}
	}
	qOut << endl;
}

void todo::details(std::stringstream &args)
{
	int index;
	args >> index;
	Task &t = tasks[index];

	qOut << "\nTask\n" << endl;
	qOut << "Title - " << t.title() << endl;
	qOut << t.description() << endl;
	qOut << "Priority - " << priorityToString(t.priority()) << " status - " << statusToString(t.status()) << endl << endl;
}

void todo::help() const
{
	qOut << "\nCommands: " << endl;
	qOut << "new <title> <description> <prioirity> where" << endl;
	qOut << "<title> - the title of the task" << endl;
	qOut << "<description> - the description of the task" << endl;
	qOut << "<priority> - low, normal or high (lower or uppercase). If ommitted, it's set to normal" << endl;
}

void todo::validateSaveFile() const
{
	/*
	 * C:/ and (wordchar 0 or more times/) 0 or more times
	 * ./ and (wordchar 0 or more times/) 0 or more times
	 * / and (wordchar 0 or more times/) 0 or more times
	 * (wordchar 0 or more times/) 0 or more times
	 *
	 * Note: the parentheses are not present in the regexp
	 * Note: wordchar is any character which might be part of a word (alfanum and _ I think)
	 */
	QRegularExpression regexp("([a-zA-Z]:/|[.]/|/)?(\\w+/)*");
	QRegularExpressionMatch match = regexp.match(savePath);
	if(!match.hasMatch())
	{
		qOut << "Regular expression or file path problem OR only file name entered (which is not a problem)" << endl;
	}
	else
	{
		QDir sp(match.captured(0));
		if(!sp.exists())
		{
			qOut << "Dir doesn't exist" << endl;
			qOut << "savePath: " << savePath << endl;
			sp.mkpath(".");

		}
	}
	QFile file(savePath);
	if(!file.open(QIODevice::Append | QIODevice::Text))
	{
		qOut << "PROBLEM" << endl;
	}
}

std::unique_ptr<QFile> todo::openSaveFile(QIODevice::OpenModeFlag flags) const
{
	validateSaveFile();
	std::unique_ptr<QFile> saveFile = std::make_unique<QFile>(savePath);
	if(!saveFile->open(flags | QIODevice::Text))
	{
		qOut << "Couldn't open save file" << endl;
		return nullptr;
	}
	return saveFile;
}
