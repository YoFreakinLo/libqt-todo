#ifndef TASK_HPP
#define TASK_HPP

#include <QString>
#include <QTextStream>

class Task
{
public:
	enum class Priority : unsigned char
	{
		LOW = 0,
		NORMAL,
		HIGH,
		COUNT
	};

	enum class Status : unsigned char
	{
		TODO = 0,
		IN_PROGRESS,
		FINISHED,
		COUNT
	};

public:
	Task();
	Task(const char *title, const char *description, Priority priority);
	Task(const QString &title, const QString &description, Priority priority);
	Task(const Task &copy);
	Task(Task &&move);

public:
	const Task &operator=(const Task &copy);
	const Task &operator=(Task &&move);
	bool operator==(const Task &with) const;

public:
	void save(QTextStream &file) const;
	void load(QTextStream &file);

public:
	Status progress();

public:
	const QString &title() const;
	const QString &description() const;
	Status status() const;
	Priority priority() const;

private:
	QString _title;
	QString _description;
	Status _status;
	Priority _priority;
};

QString priorityToString(Task::Priority);
Task::Priority stringToPriority(const QString &);
Task::Priority stringToPriority(const char *);
Task::Priority stringToPriority(const std::string &);

QString statusToString(Task::Status);

#endif // TASK_HPP
