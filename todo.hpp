#ifndef TODO_HPP
#define TODO_HPP

#include <QObject>
#include <QVector>
#include <QFile>
#include <sstream>
#include "task.hpp"
#include "libqt-todo_global.hpp"

class LIBQTTODOSHARED_EXPORT todo : public QObject
{
	Q_OBJECT
public:
	explicit todo(const char *savePath = nullptr, QObject *parent = nullptr);
	explicit todo(const QString &savePath, QObject *parent = nullptr);
	explicit todo(const std::string &savePath, QObject *parent = nullptr);
	virtual ~todo();

public:
	int run();

public:
	void save(QFile &file) const;
	void load(QFile &file);

signals:

public slots:

private:
	inline QString readCommand() const;
	void handleCommand(const QString &command);
	inline std::stringstream getArguments() const;
	QString extractArgument(std::stringstream &args) const;
	void addTask(std::stringstream &args);
	void progressTask(std::stringstream &args);
	void list() const;
	void details(std::stringstream &args);
	void help() const;
	void validateSaveFile() const;
	std::unique_ptr<QFile> openSaveFile(QIODevice::OpenModeFlag flags) const;
private:
	QVector<Task> tasks;
	bool quit;
	QString savePath;
};

#endif // TODO_HPP
