#include "task.hpp"

Task::Task() :
    _title(),
    _description(),
    _status(Status::TODO),
    _priority(Priority::NORMAL)
{

}

Task::Task(const char *title, const char *description, Priority priority) :
    _title(title),
    _description(description),
    _status(Status::TODO),
    _priority(priority)
{

}

Task::Task(const QString &title, const QString &description, Priority priority) :
    _title(title),
    _description(description),
    _status(Status::TODO),
    _priority(priority)
{

}

Task::Task(const Task &copy)
{
	*this = copy;
}

Task::Task(Task &&move)
{
	*this = std::move(move);
}

const Task &Task::operator=(const Task &copy)
{
	_title = copy._title;
	_description = copy._description;
	_status = copy._status;
	_priority = copy._priority;

	return *this;
}

const Task &Task::operator=(Task &&move)
{
	_title = move._title;
	move._title = "";
	_description = move._description;
	move._description = "";
	_status = move._status;
	_priority = move._priority;

	return *this;
}

bool Task::operator==(const Task &with) const
{
	return _title == with._title && _description == with._description && _priority == with._priority;
}

void Task::save(QTextStream &file) const
{
	file << _title << "," << _description << "," << static_cast<int>(_priority) << "," << static_cast<int>(_status) << endl;
}

void Task::load(QTextStream &file)
{
	QString line = file.readLine();
	QStringList members = line.split(',');
	_title = members[0];
	_description = members[1];
	bool successfullyConverted = false;
	Task::Priority p = static_cast<Task::Priority>(members[2].toInt(&successfullyConverted));
	if(successfullyConverted)
	{
		_priority = p;
	}
	Task::Status s = static_cast<Task::Status>(members[3].toInt(&successfullyConverted));
	if(successfullyConverted)
	{
		_status = s;
	}
}

Task::Status Task::progress()
{
	_status = static_cast<Task::Status>(static_cast<int>(_status) + 1);
	return _status;
}

const QString &Task::title() const
{
	return _title;
}

const QString &Task::description() const
{
	return _description;
}

Task::Status Task::status() const
{
	return _status;
}

Task::Priority Task::priority() const
{
	return _priority;
}


QString priorityToString(Task::Priority p)
{
	QString priority;
	switch(p)
	{
	case Task::Priority::LOW:
		priority = "LOW";
		break;
	case Task::Priority::NORMAL:
		priority = "NORMAL";
		break;
	case Task::Priority::HIGH:
		priority = "HIGH";
		break;
	}

	return priority;
}

Task::Priority stringToPriority(const QString &p)
{
	Task::Priority priority;
	std::string pr = p.toStdString();
	std::transform(pr.begin(), pr.end(), pr.begin(), toupper);
	if(pr == "LOW")
	{
		priority = Task::Priority::LOW;
	}
	else if(pr == "HIGH")
	{
		priority = Task::Priority::HIGH;
	}
	else
	{
		priority = Task::Priority::NORMAL;
	}

	return priority;
}

Task::Priority stringToPriority(const char *p)
{
	return stringToPriority(QString(p));
}

Task::Priority stringToPriority(const std::string &p)
{
	return stringToPriority(QString(p.c_str()));
}

QString statusToString(Task::Status s)
{
	QString st;
	switch(s)
	{
	case Task::Status::TODO:
		st = "TODO";
		break;
	case Task::Status::IN_PROGRESS:
		st = "IN PROGRESS";
		break;
	case Task::Status::FINISHED:
		st = "FINISHED";
		break;
	}

	return st;
}
